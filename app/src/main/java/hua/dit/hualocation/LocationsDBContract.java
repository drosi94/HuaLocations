package hua.dit.hualocation;

public class LocationsDBContract {
    public static final String DB_NAME = "locations_db";
    public static final String DB_TABLE = "locations";
    public static final String AUTHORITY = "locations_db";
    public static final String CONTENT_URI = "content://" + AUTHORITY + "/" + DB_TABLE;
    public static final String ID = "id";
    public static final String LAT = "lat";
    public static final String LON = "lon";
    public static final String TIMESTAMP = "timestamp";

    public static final String CREATE_QUERY = "CREATE TABLE " + LocationsDBContract.DB_TABLE + " ("
            + LocationsDBContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + LocationsDBContract.LAT + " REAL, " + LocationsDBContract.LON + " REAL, "
            + LocationsDBContract.TIMESTAMP + " INT)";

    public static final String DROP_QUERY = "DROP TABLE IF EXISTS " + DB_TABLE;

}
