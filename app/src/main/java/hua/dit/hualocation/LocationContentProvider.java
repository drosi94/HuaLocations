package hua.dit.hualocation;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import static hua.dit.hualocation.LocationsDBContract.AUTHORITY;
import static hua.dit.hualocation.LocationsDBContract.DB_TABLE;

public class LocationContentProvider extends ContentProvider {


    private final static int ALL_LOCATIONS = 1;
    private final static int LOCATIONS_ID = 2;
    private LocationsDBOpenHelper locationsDBOpenHelper;

    private UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    public LocationContentProvider() {
        sURIMatcher.addURI(AUTHORITY, "/locations", 1);
        sURIMatcher.addURI(AUTHORITY, "/locations/#", 1);
    }

    @Override
    public boolean onCreate() {
        locationsDBOpenHelper = new LocationsDBOpenHelper(getContext());
        return false;
    }


    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        SQLiteDatabase sqlDB = locationsDBOpenHelper.getWritableDatabase();
        long id;
        switch (sURIMatcher.match(uri)) {
            case ALL_LOCATIONS:
                id = sqlDB.insert(DB_TABLE, null, values);
                if (id == -1) {
                    Log.i("LocationContentProvider", "Something went bad");
                } else {
                    Log.i("LocationContentProvider", "Location Inserted with id " + id);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        return Uri.parse(DB_TABLE + "/" + id);
    }


    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase sqlDB = new LocationsDBOpenHelper(getContext()).getReadableDatabase();
        switch (sURIMatcher.match(uri)) {
            case 1:
                break;
            case 2:
                String id = uri.getLastPathSegment();
                selection = "_ID=?";
                selectionArgs[0] = id;
                break;
            default:
                return null;
        }
        return sqlDB.query(LocationsDBContract.DB_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
