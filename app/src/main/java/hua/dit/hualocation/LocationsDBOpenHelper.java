package hua.dit.hualocation;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static hua.dit.hualocation.LocationsDBContract.CREATE_QUERY;
import static hua.dit.hualocation.LocationsDBContract.DB_NAME;
import static hua.dit.hualocation.LocationsDBContract.DROP_QUERY;


public class LocationsDBOpenHelper extends SQLiteOpenHelper {
    public LocationsDBOpenHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(DROP_QUERY);
        onCreate(db);
    }
}
